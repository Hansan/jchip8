CC = clang++
LIBS = -lSDL2
BINDIR = bin
BIN = chip8
SRC = src/*.cpp
CFLAGS = -std=c++17 -fno-exceptions -fno-rtti -Wall -Wextra -Wpedantic -Wshadow

ifeq ($(OS),Windows_NT)
	LIBS := -lmingw32 -lSDL2main $(LIBS)
	BIN := $(BIN).exe
endif

debug:
	${CC} ${CFLAGS} -g -o ${BINDIR}/${BIN} -I. ${SRC} platform/sdlmain.cpp ${LIBS}

curses:
	${CC} ${CFLAGS} -DUSE_CURSES -g -o ${BINDIR}/${BIN} -I. ${SRC} platform/cursesmain.cpp -lpdcurses

release:
	${CC} ${CFLAGS} -O2 -o ${BINDIR}/${BIN} -I. ${SRC} platform/sdlmain.cpp ${LIBS}

clean:
	rm -i ${BINDIR}/${BIN}

syntax:
	${CC} ${CFLAGS} -fsyntax-only -I. ${SRC} platform/*.cpp
