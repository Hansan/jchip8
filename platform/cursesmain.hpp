#pragma once

#include "src/jchip.hpp"

auto curses_render(Jchip& jchip) -> void;
auto curses_poll_event(Event* event) -> bool;
auto curses_set_window_size(int w, int h) -> void;

#define render curses_render
#define poll_event curses_poll_event
#define set_window_size curses_set_window_size
