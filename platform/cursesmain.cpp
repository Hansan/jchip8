#include <pdcurses/curses.h>

#include "src/jchip.hpp"

#include "cursesmain.hpp"

auto curses_render(Jchip& jchip) -> void
{
    for (auto y = 0; y < Jchip::BBH; ++y) {
        for (auto x = 0; x < Jchip::BBW; ++x) {
            auto index = y * Jchip::BBW + x;
            auto ch = jchip.bb[index] ? '@' : ' ';

            struct Rect {
                int w;
                int h;
                int x;
                int y;
            };
            auto pixelRect = Rect {};
            pixelRect.x = x * jchip.SCALE;
            pixelRect.y = y * jchip.SCALE;
            pixelRect.w = jchip.SCALE;
            pixelRect.h = jchip.SCALE;

            /* draw_square */
            for (auto py = 0; py < pixelRect.h; ++py) {
                for (auto px = 0; px < pixelRect.w; ++px) {
                    mvaddch(y + py + 1, x + px + 1, ch);
                }
            }
        }
    }
    refresh();
}

auto curses_poll_event(Event* event) -> bool
{
    return false;
}

auto curses_set_window_size(int w, int h) -> void
{
}

auto main(int argc, char** argv) -> int
{
    if (argc != 2) {
        printf("Usage: jchip [file]\n");
        return 1;
    }

    auto jchip = Jchip {};
    jchip.load_file(argv[1]);
    jchip.SCALE = 1;

    initscr();
    noecho();

    jchip.run();
    endwin();

    return 0;
}
