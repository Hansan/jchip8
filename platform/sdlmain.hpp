#pragma once

#include "src/jchip.hpp"

auto sdl_render(Jchip& jchip) -> void;
auto sdl_poll_event(Event* event) -> bool;
auto sdl_set_window_size(int w, int h) -> void;

#define render sdl_render
#define poll_event sdl_poll_event
#define set_window_size sdl_set_window_size
