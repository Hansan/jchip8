#include <cassert>
#include <cstdio>
#include <cstdlib>
#include <ctime>
#include <string_view>

#include <jlib/jint.h>

#include <SDL2/SDL.h>

#include "src/jchip.hpp"

#include "sdlmain.hpp"

SDL_Window* gWindow = nullptr;
SDL_Surface* gWinSurface = nullptr;
SDL_Surface* gBBSurface = nullptr;

auto reset_sdl_surfaces()
{
    gWinSurface = SDL_GetWindowSurface(gWindow);
    assert(gWinSurface);

    if (gBBSurface) { SDL_FreeSurface(gBBSurface); }
    gBBSurface = SDL_CreateRGBSurface(0, gWinSurface->w, gWinSurface->h,
                                      gWinSurface->format->BitsPerPixel,
                                      gWinSurface->format->Rmask,
                                      gWinSurface->format->Gmask,
                                      gWinSurface->format->Bmask,
                                      gWinSurface->format->Amask);
    assert(gBBSurface);
}

auto init_sdl_window(Jchip& jchip)
{
    SDL_Init(SDL_INIT_EVERYTHING);

    gWindow = SDL_CreateWindow("test",
                               SDL_WINDOWPOS_UNDEFINED,
                               SDL_WINDOWPOS_UNDEFINED,
                               Jchip::BBW * jchip.SCALE, Jchip::BBH * jchip.SCALE,
                               SDL_WINDOW_SHOWN);
    assert(gWindow);

    reset_sdl_surfaces();
}

auto draw_square(SDL_Surface* surface, SDL_Rect rect, u8 color)
{
    auto startIndex = rect.y * surface->w + rect.x;
    for (auto y = 0; y < rect.h; ++y) {
        if (rect.y + y >= surface->h) continue;
        for (auto x = 0; x < rect.w; ++x) {
            if (rect.x + x >= surface->w) continue;

            auto index = startIndex + y * surface->w + x;
            auto pixel = (u8*) surface->pixels + index * surface->format->BytesPerPixel;

            *(pixel + 0) = color;
            *(pixel + 1) = color;
            *(pixel + 2) = color;
            *(pixel + 3) = 0xFF;
        }
    }
}

auto sdl_render(Jchip& jchip) -> void
{
    SDL_LockSurface(gBBSurface);
    for (auto y = 0; y < Jchip::BBH; ++y) {
        for (auto x = 0; x < Jchip::BBW; ++x) {
            auto index = y * Jchip::BBW + x;

            auto color = jchip.bb[index] ? 0xFF : 0;

            auto pixelRect = SDL_Rect {};
            pixelRect.x = x * jchip.SCALE;
            pixelRect.y = y * jchip.SCALE;
            pixelRect.w = jchip.SCALE;
            pixelRect.h = jchip.SCALE;

            draw_square(gBBSurface, pixelRect, color);
        }
    }
    SDL_UnlockSurface(gBBSurface);

    SDL_BlitSurface(gBBSurface, NULL, gWinSurface, NULL);
    SDL_UpdateWindowSurface(gWindow);
}

auto translate_sdl_event_type(Uint32 type) -> Event::Type
{
    switch (type) {
    case SDL_QUIT: {
        return Event::Type::Quit;
    } break;
    case SDL_KEYDOWN: {
        return Event::Type::KeyDown;
    } break;
    case SDL_KEYUP: {
        return Event::Type::KeyUp;
    } break;
    default: {
        return Event::Type::Unknown;
    } break;
    }
}

struct KeyTranslation {
    SDL_Keycode sdl_keycode;
    Event::Key jchip_keycode;
};

KeyTranslation keyTranslationTable[] = {
    { SDLK_0,        Event::Key::Num0 },
    { SDLK_1,        Event::Key::Num1 },
    { SDLK_2,        Event::Key::Num2 },
    { SDLK_3,        Event::Key::Num3 },
    { SDLK_4,        Event::Key::Num4 },
    { SDLK_5,        Event::Key::Num5 },
    { SDLK_6,        Event::Key::Num6 },
    { SDLK_7,        Event::Key::Num7 },
    { SDLK_8,        Event::Key::Num8 },
    { SDLK_9,        Event::Key::Num9 },
    { SDLK_a,        Event::Key::a },
    { SDLK_b,        Event::Key::b },
    { SDLK_c,        Event::Key::c },
    { SDLK_d,        Event::Key::d },
    { SDLK_e,        Event::Key::e },
    { SDLK_f,        Event::Key::f },
    { SDLK_KP_PLUS,  Event::Key::KPPlus },
    { SDLK_KP_MINUS, Event::Key::KPMinus },
    { SDLK_g,        Event::Key::g },
    { SDLK_h,        Event::Key::h },
    { SDLK_i,        Event::Key::i },
    { SDLK_j,        Event::Key::j },
    { SDLK_k,        Event::Key::k },
    { SDLK_l,        Event::Key::l },
    { SDLK_m,        Event::Key::m },
    { SDLK_n,        Event::Key::n },
    { SDLK_o,        Event::Key::o },
    { SDLK_p,        Event::Key::p },
    { SDLK_q,        Event::Key::q },
    { SDLK_r,        Event::Key::r },
    { SDLK_s,        Event::Key::s },
    { SDLK_t,        Event::Key::t },
    { SDLK_u,        Event::Key::u },
    { SDLK_v,        Event::Key::v },
    { SDLK_w,        Event::Key::w },
    { SDLK_x,        Event::Key::x },
    { SDLK_y,        Event::Key::y },
    { SDLK_z,        Event::Key::z },
    { SDLK_KP_0,     Event::Key::KP0 },
    { SDLK_KP_1,     Event::Key::KP1 },
    { SDLK_KP_2,     Event::Key::KP2 },
    { SDLK_KP_3,     Event::Key::KP3 },
    { SDLK_KP_4,     Event::Key::KP4 },
    { SDLK_KP_5,     Event::Key::KP5 },
    { SDLK_KP_6,     Event::Key::KP6 },
    { SDLK_KP_7,     Event::Key::KP7 },
    { SDLK_KP_8,     Event::Key::KP8 },
    { SDLK_KP_9,     Event::Key::KP9 },
};

auto translate_sdl_keycode(SDL_Keycode keycode) -> Event::Key
{
    for (auto translation : keyTranslationTable) {
        if (keycode == translation.sdl_keycode) {
            return translation.jchip_keycode;
        }
    }
    return Event::Key::Unknown;
}

auto sdl_poll_event(Event* event) -> bool
{
    SDL_Event sdlEvent;
    if (!SDL_PollEvent(&sdlEvent)) return false;

    event->type = translate_sdl_event_type(sdlEvent.type);
    if (event->type == Event::Type::KeyDown || event->type == Event::Type::KeyUp) {
        event->key = translate_sdl_keycode(sdlEvent.key.keysym.sym);
    }

    return true;
}

auto sdl_set_window_size(int w, int h) -> void
{
    SDL_SetWindowSize(gWindow, w, h);
    reset_sdl_surfaces();
}

auto main(int argc, char** argv) -> int
{
    if (argc != 2) {
        printf("Usage: jchip [file]\n");
        return 1;
    }

    auto jchip = Jchip {};
    jchip.load_file(argv[1]);

    init_sdl_window(jchip);

    /* jchip.do_tests(); */
    /* return 0; */

    jchip.run();

    return 0;
}
