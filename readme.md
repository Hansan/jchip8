This is a chip8 emulator.

Controls
--------

* Keypad Plus: Increase window size
* Keypad Minus: Decrease window size

Keys 0-9 and A-F map to their corresponding key register.

Dependencies
------------

* SDL2
* pdcurses (Optional)
