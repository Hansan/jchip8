#include "jchip.hpp"
#include "platform.hpp"

auto action_to_chip8_key_index(Action key)
{
    auto keyIndex = (int) key - (int) Action::Key_0;
    // TODO: maybe return an optional to avoid returning invalid index
    return keyIndex;
}

auto Jchip::set_chip8_key(Action key, bool value)
{
    auto keyIndex = action_to_chip8_key_index(key);
    if (keyIndex < 0 || keyIndex > 0xf) {
        fprintf(stderr, "Trying to set invalid key: %d", keyIndex);
    } else {
        keys[keyIndex] = value;
    }
}

struct KeyBind {
    Event::Key key;
    Action action;
};

KeyBind keyBinds[] = {
    { Event::Key::Num0,    Action::Key_0 },
    { Event::Key::Num1,    Action::Key_1 },
    { Event::Key::Num2,    Action::Key_2 },
    { Event::Key::Num3,    Action::Key_3 },
    { Event::Key::Num4,    Action::Key_4 },
    { Event::Key::Num5,    Action::Key_5 },
    { Event::Key::Num6,    Action::Key_6 },
    { Event::Key::Num7,    Action::Key_7 },
    { Event::Key::Num8,    Action::Key_8 },
    { Event::Key::Num9,    Action::Key_9 },
    { Event::Key::a,       Action::Key_10 },
    { Event::Key::b,       Action::Key_11 },
    { Event::Key::c,       Action::Key_12 },
    { Event::Key::d,       Action::Key_13 },
    { Event::Key::e,       Action::Key_14 },
    { Event::Key::f,       Action::Key_15 },
    { Event::Key::KPMinus, Action::DecreaseWindowSize },
    { Event::Key::KPPlus,  Action::IncreaseWindowSize },
    { Event::Key::q,       Action::Quit },
    { Event::Key::KP0,  Action::Key_0 },
    { Event::Key::KP1,  Action::Key_1 },
    { Event::Key::KP2,  Action::Key_2 },
    { Event::Key::KP3,  Action::Key_3 },
    { Event::Key::KP4,  Action::Key_4 },
    { Event::Key::KP5,  Action::Key_5 },
    { Event::Key::KP6,  Action::Key_6 },
    { Event::Key::KP7,  Action::Key_7 },
    { Event::Key::KP8,  Action::Key_8 },
    { Event::Key::KP9,  Action::Key_9 },
};

auto key_input_to_action(Event::Key key) -> Action
{
    for (auto bind : keyBinds) {
        if (bind.key == key) {
            return bind.action;
        }
    }
    return Action::None;
}

auto action_is_chip8_key(Action action)
{
    auto index = (int) action - (int) Action::Key_0;
    return index >= 0 && index <= 0xf;
}

auto Jchip::handle_input() -> Action
{
    // Chip-8 needs some way to know what its next keypress is.
    // If any are encountered during polling, the last one will be returned.
    auto retKey = Action {};

    auto e = Event {};
    while (poll_event(&e)) {
        if (e.type == Event::Type::Quit) {
            running = false;
        } else if (e.type == Event::Type::KeyDown) {
            auto action = key_input_to_action(e.key);
            switch (action) {
            case Action::IncreaseWindowSize: {
                ++SCALE;
                set_window_size(BBW * SCALE, BBH * SCALE);
            } break;
            case Action::DecreaseWindowSize: {
                if (SCALE > 1) {
                    --SCALE;
                    set_window_size(BBW * SCALE, BBH * SCALE);
                }
            } break;
            case Action::Key_0: case Action::Key_1:
            case Action::Key_2: case Action::Key_3:
            case Action::Key_4: case Action::Key_5:
            case Action::Key_6: case Action::Key_7:
            case Action::Key_8: case Action::Key_9:
            case Action::Key_10: case Action::Key_11:
            case Action::Key_12: case Action::Key_13:
            case Action::Key_14: case Action::Key_15: {
                retKey = action;
                set_chip8_key(action, true);
            } break;
            case Action::Quit: {
                running = false;
            } break;
            case Action::None: {
                // keybind doesn't exist or is bound to Action::None
            } break;
            default: {
                // keybind exists but has not been handled
                assert(false);
            } break;
            }
        } else if (e.type == Event::Type::KeyUp) {
            auto action = key_input_to_action(e.key);
            if (action_is_chip8_key(action)) {
                set_chip8_key(action, false);
            }
        }
    }

    return retKey;
}

// Opcodes:
// 0NNN RCA 1802 calls program at address NNN
// 00E0 Clear screen
// 00EE return from subroutine
// 1NNN jump to address NNN
// 2NNN call subroutine at NNN
// 3XNN skips next instruction if VX == NN
// 4XNN skips next instruction if VX != NN
// 5XY0 skips next instruction if VX != VY
// 6XNN VX = NN
// 7XNN VX += NN (carry flag is not changed)
// 8XY0 VX = VY
// 8XY1 VX = VX | VY
// 8XY2 VX = VX & VY
// 8XY3 VX = VX ^ VY
// 8XY4 VX += VY, VF = (carry ? 1 : 0)
// 8XY5 VX -= VY, VF = (borrow ? 0 : 1)
// 8XY6 stores the least significant bit of VX in VF and shifts VX to right by 1
// 8XY7 VX = VY - VX, VF = (borrow ? 0 : 1)
// 8XYE stores the most significant bit of VX in VF and shifts VX to left by 1
// 9XY0 skips next instruction if VX != VY
// ANNN sets I to address NNN
// BNNN jumps to the address NNN + V0
// CXNN sets VX to result of a bitwise AND operation on a random number (typically 0-255) and NN
// DXYN draws a sprite at coords VX,VY that has a w of 8 px and an h of N px.
//      each row of 8 pixels is read as bit-coded startingfrom memory location I;
//      I value doesn't change after the execution of this instruction.
//      if any pixels are flipped from set to unset, VF = 1 else VF = 0
// EX9E skips next instruction if key stored in VX is pressed
// EXA1 skips next instruction if key stored in VX isn't pressed
// FX07 sets VX to value of the delay timer
// FX0A a key press is awaited, and then stored in VX (blocking operation. all instruction halted until next key event)
// FX15 sets delay timer to VX
// FX18 sets sound timer to VX
// FX1E adds VX to I
// FX29 sets I to location of the sprite for the character in VX. Characters 0-F are represented by t 4x5 font
// FX33 stores the binary-coded decimal representation of VX, with the most significant of three digits at the address in I,
//      the middle digit at I plus 1, and the least significant digit at I plus 2.
//      (i.e take decimal representation of VX, place the hundreds digit in memory at location in I, the tens digit at location
//       I + 1, and the ones digit at location I + 2)
// FX55 stores V0-VX (including VX) in memory starting at address I. the offset from I is increased by 1 for each value written,
//      but I itself is left unmodified
// FX65 fills V0 to VX (including VX) with values from memory starting at address I. the offset from I is increased by 1 for each
//      value written, but I itself is left unmodified

auto Jchip::do_instruction(uint opcode) -> void
{
    printf("%x: %x (%x %x) - ", pc, opcode, (opcode & 0xFF00) >> 8, opcode & 0xFF);

    auto& Vf = dataReg[0xf];

    switch (opcode & 0xF000) {
    case 0x0000: {
        switch (opcode) {
        case 0x00E0: {
            for (auto& px : bb) {
                px = false;
            }
            printf("cls");
        } break;
        case 0x00EE: {
            pc = stack[--sp];
            printf("ret %x", pc);
        } break;
        default: {
            assert(false);
        } break;
        }
    } break;
    case 0x1000: {
        auto addr = opcode & 0x0FFF;
        pc = addr;
        printf("jmp %x", addr);
    } break;
    case 0x2000: {
        stack[sp++] = pc; // FIXME: make sure it's BE
        auto addr = opcode & 0x0FFF;
        pc = addr;
        printf("call %x", addr);
    } break;
    case 0x3000: {
        auto x = (opcode & 0x0F00) >> 8;
        auto& Vx = dataReg[x];
        auto val = opcode & 0x00FF;
        if (Vx == val) {
            pc += 2;
        }
        printf("skip if v%x == %x", x, val);
    } break;
    case 0x4000: {
        auto x = (opcode & 0x0F00) >> 8;
        auto& Vx = dataReg[x];
        auto val = opcode & 0x00FF;
        if (Vx != val) {
            pc += 2;
        }
        printf("skip if v%x != %x", x, val);
    } break;
    case 0x5000: {
        auto x = (opcode & 0x0F00) >> 8;
        auto y = (opcode & 0x00F0) >> 4;
        auto& Vx = dataReg[x];
        auto& Vy = dataReg[y];
        if (Vx == Vy) {
            pc += 2;
        }
        printf("skip if v%x == v%x", x, y);
    } break;
    case 0x6000: {
        auto x = (opcode & 0x0F00) >> 8;
        auto& Vx = dataReg[x];
        auto val = opcode & 0x00FF;
        Vx = val;
        printf("set v%x to %x", x, val);
    } break;
    case 0x7000: {
        auto x = (opcode & 0x0F00) >> 8;
        auto& Vx = dataReg[x];
        auto val = opcode & 0x00FF;
        Vx += val;
        printf("add %x to v%x", val, x);
    } break;
    case 0x8000: {
        auto x = (opcode & 0x0F00) >> 8;
        auto y = (opcode & 0x00F0) >> 4;
        auto& Vx = dataReg[x];
        auto& Vy = dataReg[y];
        switch (opcode & 0x000F) {
        case 0x0000: {
            Vx = Vy;
            printf("set v%x to v%x", x, y);
        } break;
        case 0x0001: {
            Vx |= Vy;
            printf("OR v%x and v%x", x, y);
        } break;
        case 0x0002: {
            Vx &= Vy;
            printf("AND v%x and v%x", x, y);
        } break;
        case 0x0003: {
            Vx ^= Vy;
            printf("XOR v%x and v%x", x, y);
        } break;
        case 0x0004: {
            auto result = int { Vx + Vy };
            if (result > 255) {
                Vf = 1;
                result -= 256;
            } else {
                Vf = 0;
            }
            Vx = result;
            printf("add v%x to v%x", y, x);
        } break;
        case 0x0005: {
            auto result = int { Vx - Vy };
            if (result < 0) {
                Vf = 0;
                result += 256;
            } else {
                Vf = 1;
            }
            Vx = result;
            printf("sub v%x from v%x", y, x);
        } break;
        case 0x0006: {
            Vf = Vx & 1;
            Vx >>= 1;
            printf("store least significant bit of v%x", x);
        } break;
        case 0x0007: {
            auto result = int { Vy - Vx };
            if (result < 0) {
                Vf = 0;
                result += 256;
            } else {
                Vf = 1;
            }
            Vx = result;
            printf("set v%x to v%x minus v%x", x, y, x);
        } break;
        case 0x000E: {
            Vf = (Vx & (1u << 7)) ? 1 : 0;
            Vx <<= 1;
            printf("store most significant bit of v%x", x);
        } break;
        default: {
            assert(false);
        } break;
        }
    } break;
    case 0x9000: {
        auto x = (opcode & 0x0F00) >> 8;
        auto y = (opcode & 0x00F0) >> 4;
        auto& Vx = dataReg[x];
        auto& Vy = dataReg[y];
        if (Vx != Vy) {
            pc += 2;
        }
        printf("skip if v%x != v%x", x, y);
    } break;
    case 0xA000: {
        auto addr = opcode & 0x0FFF;
        I = addr;
        printf("set I to %x", addr);
    } break;
    case 0xB000: {
        auto addr = opcode & 0x0FFF;
        pc = addr + dataReg[0];
        printf("set pc to %x + v0", addr);
    } break;
    case 0xC000: {
        auto rng = rand() % 256; // TODO: seed
        auto x = (opcode & 0x0F00) >> 8;
        auto& Vx = dataReg[x];
        auto val = opcode & 0x00FF;
        auto result = val & rng;
        Vx = result;
        printf("set v%x to rng AND %x", x, val);
    } break;
    case 0xD000: {
        Vf = 0;

        auto x = (opcode & 0x0F00) >> 8;
        auto y = (opcode & 0x00F0) >> 4;
        auto& Vx = dataReg[x];
        auto& Vy = dataReg[y];
        auto n = (opcode & 0x000F);
        // sprites are always 8 px wide represented as bitfield in u8
        auto w = 8u;
        auto h = n;

        for (auto spriteY = 0u; spriteY < h; ++spriteY) {
            auto spriteLoc = I + spriteY;
            for (auto spriteX = 0u; spriteX < w; ++spriteX) {
                auto xShiftAmount = w - 1u - spriteX;
                bool spritePixel = memory[spriteLoc] & (0x1 << xShiftAmount);
                auto bbY = Vy + spriteY;
                auto bbX = Vx + spriteX;
                // wrap sprite around screen
                if (bbY >= BBH) {
                    bbY -= BBH;
                }
                if (bbX >= BBW) {
                    bbX -= BBW;
                }
                auto bbLoc = bbY * BBW + bbX;

                // set or unset bb pixel
                bb[bbLoc] ^= spritePixel;

                // if bb pixel was unset, set f reg
                if (spritePixel && bb[bbLoc] == false) {
                    Vf = 1;
                }
            }
        }

        printf("display sprite at %d,%d", Vx, Vy);
    } break;
    case 0xE000: {
        auto x = (opcode & 0x0F00) >> 8;
        auto& Vx = dataReg[x];
        auto keyPressed = keys[Vx];
        auto skipped = false;
        switch (opcode & 0x00FF) {
        case 0x009E: {
            if (keyPressed) {
                skipped = true;
                pc += 2;
            }
            printf("skip if key %x down %s", Vx, skipped ? "(true)" : "(false)");
        } break;
        case 0x00A1: {
            if (!keyPressed) {
                skipped = true;
                pc += 2;
            }
            printf("skip if key %x up %s", Vx, skipped ? "(true)" : "(false)");
        } break;
        default: {
            assert(false); // shouldn't happen
        } break;
        }
    } break;
    case 0xF000: {
        auto x = (opcode & 0x0F00) >> 8;
        auto& Vx = dataReg[x];
        switch (opcode & 0x00FF) {
        case 0x0007: {
            Vx = delayTimer;
            printf("set v%x to delay timer(%x)", x, delayTimer);
        } break;
        case 0x000A: {
            printf("set v%x to next key press (0-F)", x);
            auto lastKeyDown = Action {};
            do {
                lastKeyDown = handle_input();
            } while (!action_is_chip8_key(lastKeyDown));

            auto index = action_to_chip8_key_index(lastKeyDown);
            Vx = index;
        } break;
        case 0x0015: {
            delayTimer = Vx;
            printf("set delay timer to v%x", x);
        } break;
        case 0x0018: {
            soundTimer = Vx;
            printf("set sound timer to v%x", x);
        } break;
        case 0x001E: {
            // Undocumented feature: when there is overflow (I + VX > 0x0FFF) VF is set to 1, otherwise 0;
            auto value = Vx;
            auto IVal = I;
            auto result = int {value + IVal};
            if (result > 0x0FFF) {
                result -= 0x1000;
                // FIXME: should reg f be set here or not?
                /* Vf = 1; */
            } else {
                /* Vf = 0; */
            }
            I = result;
            printf("add v%x to I", x);
        } break;
        case 0x0029: {
            auto value = Vx;
            auto spriteStartLoc = 0; // assumes sprites start at mem loc 0
            auto location = spriteStartLoc + value * 5; // sprites are 5 bytes in size
            I = location;
            printf("set I to location of sprite with index in v%x", x);
        } break;
        case 0x0033: {
            auto value = Vx;
            // TODO: check this is actually correct. might be better way to do it.
            auto hundreds = value / 100;
            auto tens = value % 100 / 10;
            auto ones = value % 10;
            memory[I] = hundreds;
            memory[I + 1] = tens;
            memory[I + 2] = ones;
            printf("set hundreds, tens, and ones of v%x in memory[I, I+1, I+2]", x);
        } break;
        case 0x0055: {
            for (auto i = 0u; i <= x; ++i) {
                memory[I + i] = dataReg[i];
            }
            printf("set memory[I..I+%x] to v0-v%x", x, x);
        } break;
        case 0x0065: {
            for (auto i = 0u; i <= x; ++i) {
                dataReg[i] = memory[I + i];
            }
            printf("set v0-v%x to memory[I..I+%x]", x, x);
        } break;
            // shouldn't happen
        default: {
            assert(0);
        } break;
        }
    } break;
        // shouldn't happen
    default: {
        assert(0);
    } break;
    }

    // tmp
    printf("\n");
}

auto Jchip::do_tests() -> void
{
    // TODO: assert flags change during instructions
    for (auto& pixel : bb) {
        pixel = true;
    }
    do_instruction(0x00E0);
    for (auto pixel : bb) {
        assert(!pixel);
    }

    // 0x1nnn
    do_instruction(0x1100);
    assert(pc == 0x100);
    do_instruction(0x1200);
    assert(pc == 0x200);
    do_instruction(0x1F5C);
    assert(pc == 0xF5C);

    // 0x2nnn
    assert(sp == 0);
    do_instruction(0x28AD);
    assert(pc == 0x8AD);
    assert(sp == 1);

    // 0x00ee
    do_instruction(0x00EE);
    assert(pc == 0xF5C);
    assert(sp == 0);

    assert(dataReg[0x0] == 0);
    do_instruction(0x6012);
    assert(dataReg[0x0] == 0x12);
    assert(dataReg[0x1] == 0);
    do_instruction(0x6112);
    assert(dataReg[0x1] == 0x12);
    assert(dataReg[0xF] == 0);
    do_instruction(0x6FA9);
    assert(dataReg[0xF] == 0xA9);

    auto oldpc = pc;
    do_instruction(0x3FA9);
    assert(pc == oldpc + 2);
    oldpc = pc;
    do_instruction(0x3FB9);
    assert(pc == oldpc);

    oldpc = pc;
    do_instruction(0x41FE);
    assert(pc == oldpc + 2);
    oldpc = pc;
    do_instruction(0x4112);
    assert(pc == oldpc);

    oldpc = pc;
    do_instruction(0x5010);
    assert(pc == oldpc + 2);
    oldpc = pc;
    do_instruction(0x5100);
    assert(pc == oldpc + 2);
    oldpc = pc;
    do_instruction(0x51F0);
    assert(pc == oldpc);

    assert(dataReg[1] == 0x12);
    do_instruction(0x714A);
    assert(dataReg[1] == 0x12 + 0x4A);

    assert(dataReg[0xF] == 0xA9);
    do_instruction(0x8F10);
    assert(dataReg[0xF] == 0x12 + 0x4A);

    // 8XY1 VX = VX | VY
    dataReg[0x0] = 1;
    dataReg[0x1] = 2;
    do_instruction(0x8011);
    assert(dataReg[0x0] == 3);
    assert(dataReg[0x1] == 2);

    // 8XY2 VX = VX & VY
    dataReg[0x0] = 3;
    dataReg[0x1] = 2;
    do_instruction(0x8012);
    assert(dataReg[0x0] == 2);
    assert(dataReg[0x1] == 2);

    // 8XY3 VX = VX ^ VY
    dataReg[0x0] = 5;
    dataReg[0x1] = 6;
    do_instruction(0x8013);
    assert(dataReg[0x0] == 3);
    assert(dataReg[0x1] == 6);

    // 8XY4 VX += VY, VF = (carry ? 1 : 0)
    dataReg[0x0] = 2;
    dataReg[0x1] = 34;
    dataReg[0xf] = 1;
    do_instruction(0x8014);
    assert(dataReg[0x0] == 36);
    assert(dataReg[0x1] == 34);
    assert(dataReg[0xf] == 0); // non-carry check

    dataReg[0x0] = 100;
    dataReg[0x1] = 200;
    dataReg[0xf] = 0;
    do_instruction(0x8014);
    assert(dataReg[0x0] == 44);
    assert(dataReg[0x1] == 200);
    assert(dataReg[0xf] == 1); // carry check

    // 8XY5 VX -= VY, VF = (borrow ? 0 : 1)
    dataReg[0x0] = 4;
    dataReg[0x1] = 2;
    dataReg[0xf] = 0;
    do_instruction(0x8015);
    assert(dataReg[0x0] == 2);
    assert(dataReg[0x1] == 2);
    assert(dataReg[0xf] == 1); // NOT borrow check

    dataReg[0x0] = 5;
    dataReg[0x1] = 10;
    dataReg[0xf] = 1;
    do_instruction(0x8015);
    assert(dataReg[0x0] == 251);
    assert(dataReg[0x1] == 10);
    assert(dataReg[0xf] == 0); // borrow check

    // 8XY6 stores the least significant bit of VX in VF and shifts VX to right by 1
    dataReg[0xf] = 0;
    dataReg[0x0] = 3;
    do_instruction(0x8006);
    assert(dataReg[0xf] == 1);
    assert(dataReg[0x0] == 1);
    dataReg[0xf] = 0;
    dataReg[0x0] = 1;

    do_instruction(0x8006);
    assert(dataReg[0xf] == 1);
    assert(dataReg[0x0] == 0);
    dataReg[0xf] = 0;
    dataReg[0x0] = 2;
    do_instruction(0x8006);
    assert(dataReg[0xf] == 0);
    assert(dataReg[0x0] == 1);

    // 8XY7 VX = VY - VX, VF = (borrow ? 0 : 1)
    dataReg[0x0] = 2;
    dataReg[0x1] = 4;
    dataReg[0xf] = 0;
    do_instruction(0x8017);
    assert(dataReg[0x0] == 2);
    assert(dataReg[0x1] == 4);
    assert(dataReg[0xf] == 1); // NOT borrow check

    dataReg[0x0] = 106;
    dataReg[0x1] = 6;
    dataReg[0xf] = 0;
    do_instruction(0x8017);
    printf("%d\n", dataReg[0x0]);
    assert(dataReg[0x0] == 156);
    assert(dataReg[0x1] == 6);
    assert(dataReg[0xf] == 0); // NOT borrow check

    // 8XYE stores the most significant bit of VX in VF and shifts VX to left by 1
    dataReg[0xf] = 0;
    dataReg[0x0] = 1u << 7;
    do_instruction(0x800e);
    assert(dataReg[0xf] == 1);
    assert(dataReg[0x0] == 0);
    dataReg[0xf] = 0;
    dataReg[0x0] = 1u << 6;
    do_instruction(0x800e);
    assert(dataReg[0xf] == 0);
    assert(dataReg[0x0] == (1u << 7));
    dataReg[0xf] = 0;
    dataReg[0x0] = (1u << 7) + (1u << 6);
    do_instruction(0x800e);
    assert(dataReg[0xf] == 1);
    assert(dataReg[0x0] == (1u << 7));

    // 9XY0 skips next instruction if VX != VY
    dataReg[0x0] = 0;
    dataReg[0x1] = 1;
    oldpc = pc;
    do_instruction(0x9010);
    assert(oldpc == pc - 2);
    dataReg[0x0] = 1;
    dataReg[0x1] = 1;
    oldpc = pc;
    do_instruction(0x9010);
    assert(oldpc == pc);

    // ANNN sets I to address NNN
    I = 0;
    do_instruction(0xA123);
    assert(I == 0x123);

    // BNNN jumps to the address NNN + V0
    dataReg[0x0] = 6;
    assert(pc != (0x123 + 6));
    do_instruction(0xB123);
    assert(pc == (0x123 + 6));

    // CXNN sets VX to result of a bitwise AND operation on a random number (typically 0-255) and NN

    // DXYN draws a sprite at coords VX,VY that has a w of 8 px and an h of N px.
    //      each row of 8 pixels is read as bit-coded startingfrom memory location I;
    //      I value doesn't change after the execution of this instruction.
    //      if any pixels are flipped from set to unset, VF = 1 else VF = 0

#if 1
    // TODO: requires input handling to be available.
    //       maybe detect this somehow?

    // FX0A a key press is awaited, and then stored in VX (blocking operation. all instruction halted until next key event)
    fprintf(stderr, "Press '0' button\n");
    do_instruction(0xf00a);
    assert(dataReg[0x0] == 0);
    fprintf(stderr, "Press '1' button\n");
    do_instruction(0xf00a);
    assert(dataReg[0x0] == 1);
    fprintf(stderr, "Press '2' button\n");
    do_instruction(0xf00a);
    assert(dataReg[0x0] == 2);
    fprintf(stderr, "Press 'a' button\n");
    do_instruction(0xf00a);
    assert(dataReg[0x0] == 0xa);
    fprintf(stderr, "Press 'e' button\n");
    do_instruction(0xf00a);
    assert(dataReg[0x0] == 0xe);

    // EX9E skips next instruction if key stored in VX is pressed
    // EXA1 skips next instruction if key stored in VX isn't pressed
#endif
    // FX07 sets VX to value of the delay timer
    delayTimer = 42;
    dataReg[0x0] = 0;
    do_instruction(0xf007);
    assert(dataReg[0x0] == delayTimer);

    // FX15 sets delay timer to VX
    delayTimer = 0;
    dataReg[0x0] = 68;
    do_instruction(0xf015);
    assert(dataReg[0x0] == delayTimer);

    // FX18 sets sound timer to VX
    soundTimer = 0;
    dataReg[0x0] = 42;
    do_instruction(0xf018);
    assert(soundTimer == dataReg[0x0]);

    // FX1E adds VX to I
    auto oldI = I;
    dataReg[0x0] = 4;
    do_instruction(0xf01e);
    assert(I == oldI + dataReg[0x0]);

    // FX29 sets I to location of the sprite for the character in VX. Characters 0-F are represented by t 4x5 font
    auto spriteSize = 5;
    dataReg[0x0] = 0;
    do_instruction(0xf029);
    assert(I == 0);
    dataReg[0x1] = 1;
    do_instruction(0xf129);
    assert(I == 1 * spriteSize);
    dataReg[0xa] = 0xa;
    do_instruction(0xfa29);
    assert(I == 0xa * spriteSize);
    dataReg[0xf] = 0xf;
    do_instruction(0xff29);
    assert(I == 0xf * spriteSize);

    // FX33 stores the binary-coded decimal representation of VX, with the most significant of three digits at the address in I,
    //      the middle digit at I plus 1, and the least significant digit at I plus 2.
    //      (i.e take decimal representation of VX, place the hundreds digit in memory at location in I, the tens digit at location
    //       I + 1, and the ones digit at location I + 2)
    // FX55 stores V0-VX (including VX) in memory starting at address I. the offset from I is increased by 1 for each value written,
    //      but I itself is left unmodified
    // FX65 fills V0 to VX (including VX) with values from memory starting at address I. the offset from I is increased by 1 for each
    //      value written, but I itself is left unmodified
}

auto Jchip::get_opcode() -> u16
{
    auto opcode = memory[pc] << 8 | memory[pc + 1];
    pc += 2;
    return opcode;
}

auto Jchip::load_file(std::string_view filename) -> void
{
    auto file = fopen(filename.data(), "rb");
    assert(file);
    fseek(file, 0, SEEK_END);
    auto count = ftell(file);
    // make sure the program fits in RAM
    assert(count <= (0xFFF - pc));
    fseek(file, 0, SEEK_SET);
    fread(memory + pc, 1, count, file);
}

auto Jchip::tick_timers() -> void
{
    auto currclock = clock();
    while (currclock > nextclock) {
        nextclock += CLOCKS_PER_TIMER_UPDATE;
        if (delayTimer > 0) {
            --delayTimer;
        }
        if (soundTimer > 0) {
            --soundTimer;
        }
    }
}

auto Jchip::do_next_instruction() -> void
{
    do_instruction(get_opcode());
    tick_timers();
}

auto Jchip::run() -> void
{
    while (running) {
        do_next_instruction();
        handle_input();
        printf("\n");
        render(*this);
    }
}
