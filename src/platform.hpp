#pragma once

#if defined(USE_CURSES)
# include "platform/cursesmain.hpp"
#else
# include "platform/sdlmain.hpp"
#endif
