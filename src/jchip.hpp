#pragma once

#include <cstdlib>
#include <ctime>
#include <cstdio>
#include <cassert>
#include <string_view>

#include <jlib/jint.h>

struct Event {
    enum class Type {
        Unknown,
        Quit,
        KeyDown,
        KeyUp,
    };
    enum class Key {
        Unknown,
        KPMinus, KPPlus,
        // must be in order for getting index by casting to int to work
        Num0, Num1, Num2, Num3, Num4, Num5, Num6, Num7, Num8, Num9,
        a, b, c, d, e, f,
        g, h, i, j, k, l, m, n, o, p, q, r, s, t, u, v, w, x, y, z,
        KP0, KP1, KP2, KP3, KP4, KP5, KP6, KP7, KP8, KP9,
    };
    Type type;
    Key key;
};

enum class Action {
    None,
    // Note: Keep all keys in order
    Key_0, Key_1, Key_2, Key_3, Key_4, Key_5, Key_6, Key_7,
    Key_8, Key_9, Key_10, Key_11, Key_12, Key_13, Key_14, Key_15,
    IncreaseWindowSize,
    DecreaseWindowSize,
    Quit,
};

class Jchip {
public:
    int SCALE = 10;
    const static auto BBW = 64;
    const static auto BBH = 32;
    const static auto TIMER_UPDATE_FREQ = 60;
    const static auto CLOCKS_PER_TIMER_UPDATE = CLOCKS_PER_SEC / TIMER_UPDATE_FREQ;

    bool bb[BBW * BBH] = {};

private:
    // buffers
    u16 stack[16] = {};
    bool keys[0x10] = {};

    u8 memory[0x1000] = {
        0xF0, 0x90, 0x90, 0x90, 0xF0, // 0 sprite
        0x20, 0x60, 0x20, 0x20, 0x70, // 1 sprite
        0xF0, 0x10, 0xF0, 0x80, 0xF0, // 2 sprite
        0xF0, 0x10, 0xF0, 0x10, 0xF0, // 3 sprite
        0x90, 0x90, 0xF0, 0x10, 0x10, // 4 sprite
        0xF0, 0x80, 0xF0, 0x10, 0xF0, // 5 sprite
        0xF0, 0x80, 0xF0, 0x90, 0xF0, // 6 sprite
        0xF0, 0x10, 0x20, 0x40, 0x40, // 7 sprite
        0xF0, 0x90, 0xF0, 0x90, 0xF0, // 8 sprite
        0xF0, 0x90, 0xF0, 0x10, 0xF0, // 9 sprite
        0xF0, 0x90, 0xF0, 0x90, 0x90, // A sprite
        0xE0, 0x90, 0xE0, 0x90, 0xE0, // B sprite
        0xF0, 0x80, 0x80, 0x80, 0xF0, // C sprite
        0xE0, 0x90, 0x90, 0x90, 0xE0, // D sprite
        0xF0, 0x80, 0xF0, 0x80, 0xF0, // E sprite
        0xF0, 0x80, 0xF0, 0x80, 0x80, // F sprite
    };

    // counters
    u8 sp = 0;
    u16 pc = 0x200;

    // registers
    u8 dataReg[0x10] = {};
    u16 I = 0;

    // timers
    u8 delayTimer = 0;
    u8 soundTimer = 0;

public:

    // FIXME: using clock() doesn't work while using tmux in tty mode
    clock_t startclock = clock();
    clock_t nextclock = startclock + CLOCKS_PER_TIMER_UPDATE;
    bool running = true;

    Jchip() {
        srand(time(NULL));
    }

    auto set_chip8_key(Action key, bool value);
    auto handle_input() -> Action;
    auto do_instruction(uint opcode) -> void;
    auto do_tests() -> void;
    auto get_opcode() -> u16;
    auto load_file(std::string_view filename) -> void;
    auto tick_timers() -> void;
    auto do_next_instruction() -> void;
    auto run() -> void;
};
